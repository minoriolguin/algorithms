/*
 * Assignment 2 - Exercise 5
 * Minori Olguin
 * UCID: 30035923
 * 
 * Description: 
 * 
 * Limitation: 
 * 
 */

import java.lang.reflect.Array;

public class AsteroidCollisionsStack<T extends Comparable<T>> {
	private T[] stack;
	private int topIndex;

	// Constructor for Stack, creates an empty stack
	// @param class Component type
	// @param int maxSize maximum size of stack
	@SuppressWarnings("unchecked")
	public AsteroidCollisionsStack(Class<T> clazz, int maxSize) {
		stack = (T[]) Array.newInstance(clazz, maxSize);
		topIndex = -1;
	}

	// Returns a boolean value for if the stack is empty
	public boolean isEmpty() {
		return topIndex == -1;
	}

	// Returns a boolean value for if the stack is full
	public boolean isFull() {
		return topIndex == (stack.length - 1);
	}

	// Add an item to the top of the stack
	public void push(T item) throws OverflowException {
		if (!isFull()) {
			topIndex++;
			stack[topIndex] = item;
		} else {
			throw new OverflowException("The stack is full, cannot push an element to a full stack.");
		}
	}

	// Remove an item from the top of the stack
	public T pop() throws UnderflowException {
		if (!isEmpty()) {
			T tmp = stack[topIndex];
			stack[topIndex] = null;
			topIndex--;
			return tmp;
		} else {
			throw new UnderflowException("The stack is empty, cannot pop an element from an empty stack.");
		}
	}

	// Show the top value of the stack
	public T peek() throws UnderflowException {
		if (!isEmpty()) {
			return stack[topIndex];
		} else {
			throw new UnderflowException("The stack is empty, cannot show top item.");
		}
	}

	// Clear the stack
	public void clear() {
		stack = null;
		topIndex = -1;
	}

	public int size() {
		return topIndex + 1;
	}

	public void reverseStack(AsteroidCollisionsStack<Integer> inputStack, Queue<Integer> queue) {
		// Step 1: Pop elements from the stack and enqueue into the queue
		while (!inputStack.isEmpty()) {
			Integer element = inputStack.pop();
			queue.enqueue(element);
		}

		// Step 2: Dequeue elements from the queue and push into the stack
		while (!queue.isEmpty()) {
			Integer element = queue.dequeue();
			inputStack.push(element);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (int i = 0; i <= topIndex; i++) {
			sb.append(stack[i]);
			if (i < topIndex) {
				sb.append(", ");
			}
		}
		sb.append(']');
		return sb.toString();
	}

	public void simulateCollisions(AsteroidCollisionsStack<Integer> asteroids) {
		int curr = 0;
		int n = 1;
		int size = asteroids.size();
		AsteroidCollisionsStack<Integer> resultStack = new AsteroidCollisionsStack<>(Integer.class, size);


		for (int i = 0; i < size; i++) {
			System.out.println("Step-" + n + ": " + resultStack.toString() + "\n");

			curr = asteroids.pop();
			if (resultStack.isEmpty() || (curr >= 0 && resultStack.peek() >= 0) || (curr < 0 && resultStack.peek() < 0)) {
				resultStack.push(curr);
			}
			else if (curr >= 0 && resultStack.peek() < 0) {
				if (curr + resultStack.peek() > 0) {
					resultStack.pop();
					resultStack.push(curr);
				} 
				else if (curr + resultStack.peek() == 0) {
					resultStack.pop();
				}
			}
			else if (curr < 0 && resultStack.peek() >= 0) {
				if (curr + resultStack.peek() < 0) {
					resultStack.pop();
					resultStack.push(curr);
				} 
				else if (curr + resultStack.peek() == 0) {
					resultStack.pop();
				}
			}

			n++;
		}

		System.out.println("Step-" + n + ": Final asteroid = " + resultStack.toString() + "\n\n");
	}

	public static void main(String[] args) {
		
		// Hardcoded input for the stack 
		int[] inputAstroids = { 7, 16, -16, -7, 5, -5, -21, 2, 2, 34, -9 };
		int maxSize = inputAstroids.length;

		// Initializing the result stack 
		AsteroidCollisionsStack<Integer> inputStack = new AsteroidCollisionsStack<>(Integer.class, maxSize);
		// AsteroidCollisionsStack<Integer> resultStack = new AsteroidCollisionsStack<>(Integer.class, maxSize);
		Queue<Integer> queue = new Queue<Integer>();


		// Adding all the hardcoded integers to the stack 
		for (int i : inputAstroids) {
			inputStack.push(i);
		}

		// Print title and the starting stack
		System.out.println("\nExercise 5: Astroid Collisions Stack\n");
		System.out.println("Input asteroids = " + inputStack.toString() + "\n");

		inputStack.reverseStack(inputStack, queue);
		inputStack.simulateCollisions(inputStack);
	}
}

class OverflowException extends RuntimeException {
	public OverflowException(String message) {
		super(message);
	}
}

class UnderflowException extends RuntimeException {
	public UnderflowException(String message) {
		super(message);
	}

}

// Class Queue<T>, implements a queue ADT
class Queue<T extends Comparable<T>> {

	@SuppressWarnings("hiding")
	private class Node<T> {
		private T value;
		private Node<T> next;
	}

	private Node<T> front = null;
	private Node<T> rear = null;

	// @param T item
	public void enqueue(T item) {
		Node<T> newNode = new Node<T>();
		newNode.value = item;
		newNode.next = null; 

		if (rear != null) {
			rear.next = newNode; // Link the current rear to the new node.
		}
		rear = newNode;

		if (front == null) {
			front = newNode;
		}
	}

	// @returns tmp, the front value of the queue at time of function call
	public T dequeue() {
		if (front == null) {
			return null;
		}

		T tmp = front.value;
		front = front.next;
		return tmp;
	}

	public boolean isEmpty() {
		return front == null;

	}

	public boolean isFull() {
		return false;
	}

	public int size() {
		int size = 0;
		Node<T> current = front;
		while (current != null) {
			size++;
			current = current.next;
		}
		return size;
	}

	// @param T item 
	public boolean contains(T item) {
		Node<T> current = front; // Set current place to the front of the queue
		while (current != null) {
			if (current.value.equals(item)) {
				return true; // Item found
			}
			current = current.next; // Move to the next node
		}
		return false; // Item not found
	}

	// Method to print the queue
	public String printQueue() {
		Node<T> current = front; // Set current place to front of the queue
		StringBuilder sb = new StringBuilder(); // Using string builder to produce desired format
		sb.append("[ "); // Start queue with [

		while (current != null) {
			sb.append(current.value); // Add current value to the String 
			if (current.next != null) {
				sb.append(" - "); // Use a '-' in between each item  
			}
			current = current.next; // Move to the next node
		}

		sb.append(" ]"); // End queue with ]
		return sb.toString();
	}

}
