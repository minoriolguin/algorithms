
/*
 * Assignment 2 - Exercise 4
 * Minori Olguin
 * UCID: 30035923
 * 
 * Description: 
 * 
 * Limitation: 
 */

import java.lang.reflect.Array;

// two imports only used for the creation of a dictionary
import java.util.ArrayList;
import java.util.List;

public class ShoppingCartStack<T> {
    private T[] shoppingCart;
    private int topIndex;

    @SuppressWarnings("unchecked")
    public ShoppingCartStack(Class<T> clazz, int maxSize) {
        T[] shoppingCart = (T[]) Array.newInstance(clazz, maxSize);
        this.shoppingCart = shoppingCart;
        topIndex = -1;
    }

    // Returns a boolean value for if the stack is empty
    public boolean isEmpty() {
        return topIndex == -1;
    }

    // Returns a boolean value for if the stack is full
    public boolean isFull() {
        return topIndex == (shoppingCart.length - 1);
    }

    // Add an item to the top of the stack
    public void push(T item) throws OverflowException {
        if (!isFull()) {
            topIndex++;
            shoppingCart[topIndex] = item;
        } else {
            throw new OverflowException("The stack is full, cannot push an element to a full stack.");
        }
    }

    // Remove an item from the top of the stack
    public T pop() throws UnderflowException {
        if (!isEmpty()) {
            T tmp = shoppingCart[topIndex];
            shoppingCart[topIndex] = null;
            topIndex--;
            return tmp;
        } else {
            throw new UnderflowException("The stack is empty, cannot pop an element from an empty stack.");
        }
    }

    // Show the top value of the stack
    public T peek() throws UnderflowException {
        if (!isEmpty()) {
            return shoppingCart[topIndex];
        } else {
            throw new UnderflowException("The stack is empty, cannot show top item.");
        }
    }

    // Clear the stack
    public void clear() {
        shoppingCart = null;
        topIndex = -1;
    }

    // Returns size of the stack
    public int size() {
        return topIndex + 1;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        for (int i = 0; i <= topIndex; i++) {
            sb.append(shoppingCart[i]);
            if (i < topIndex) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }

    public static void main(String[] args) {

        String[] stepsArray = { "1st step: ", "2nd step: ", "3rd step: ", "4th step: ", "5th step: ", "6th step: " };

        // Initializing Shoe data
        ShoeData nike = new ShoeData("Nike", 450, 15);
        ShoeData adidas = new ShoeData("Adidas", 400, 10);
        ShoeData puma = new ShoeData("Puma", 600, 30);
        ShoeData sorel = new ShoeData("Sorel", 360, 0);
        ShoeData aldo = new ShoeData("Aldo", 680, 15);
        ShoeData sketchers = new ShoeData("Sketchers", 390, 0);

        // Initializing dictionary to store shoe data
        Dictionary dictionary = new Dictionary();

        // Adding shoe data to dictionary
        dictionary.addShoe(nike);
        dictionary.addShoe(adidas);
        dictionary.addShoe(puma);
        dictionary.addShoe(sorel);
        dictionary.addShoe(aldo);
        dictionary.addShoe(sketchers);

        System.out.println("\nExercise 4: Shopping Cart Stack\n");
        System.out.println("Shoes info: " + dictionary.toString() + "\n");

        // Initialize the shoppingCart stack
        @SuppressWarnings({ "unchecked", "rawtypes" })
        ShoppingCartStack<ShoeData> shoppingCart = new ShoppingCartStack(ShoeData.class, dictionary.size());

        // Pushing the first item in the dictionary to the shopping cart stack, peek at
        // the stack
        shoppingCart.push(dictionary.getShoeData(0));
        System.out.println(stepsArray[0] + "[" + shoppingCart.peek() + "]\n");

        ShoeData prevShoe = null;
        ShoeData nextShoe = null;
        ShoeData temp = null;
        int prev = 0;

        if (!shoppingCart.isEmpty()) {
            prev = shoppingCart.peek().getDiscountPrice(); // Getting discounted item price from top item of stack
            prevShoe = shoppingCart.pop(); // Pop top item from shopping cart, temporarily store in current shoe
        }

        for (int i = 1; i < dictionary.size(); i++) {
            shoppingCart.push(dictionary.getShoeData(i));
            int next = shoppingCart.peek().getDiscountPrice();
            if (prev > next) {
                nextShoe = shoppingCart.peek();
    
                System.out.println(
                        stepsArray[i] + "[" + nextShoe + "] as " + nextShoe.getPrice() + " X " + nextShoe.getDiscount()
                                + " percent = " + nextShoe.getDiscountPrice() + " is less than "
                                + prevShoe.getDiscountPrice() + " \n");

                prev = shoppingCart.peek().getDiscountPrice(); // Getting discounted item price from top item of stack
                prevShoe = shoppingCart.pop(); // Pop top item from shopping cart, temporarily store in prev shoe

            } else if (prev < next) {
                temp = shoppingCart.pop(); // Pop the shoe with a higher price from the shopping cart 

                shoppingCart.push(prevShoe); // Push the shoe with a lower price to the shopping cart
                nextShoe = shoppingCart.peek(); // Set nextShoe variable to represent the shoe with a lower price by peeking at stack 

                prevShoe = temp;  // Set prev shoe to temp, temp is the shoe with the higher price poped from stack 

                System.out.println(stepsArray[i] + "[" + nextShoe + "] as " + prevShoe.getPrice() + " X " + prevShoe.getDiscount()
                        + " percent = " + prevShoe.getDiscountPrice() + " is greater than "
                        + nextShoe.getDiscountPrice() + " \n");
                
                prev = shoppingCart.peek().getDiscountPrice(); // Getting discounted item price from top item of stack
                prevShoe = shoppingCart.pop();  // popping the shoe from the stack for the next compairison

            } else {
                if (prevShoe.getDiscount() < nextShoe.getDiscount()) { // If the previous shoe's discount is less than the next shoe's discount
                    nextShoe = shoppingCart.peek();

                } else {  // If the the next shoe's discount is less than or equal to the previous shoe's discount
                    temp = shoppingCart.pop();
                    shoppingCart.push(prevShoe);
                    nextShoe = shoppingCart.peek();
                    prevShoe = temp;
                }
                System.out.println(
                        stepsArray[i] + "[" + nextShoe + "] as " + prevShoe.getPrice() + " X " + prevShoe.getDiscount()
                                + " percent = " + prevShoe.getDiscountPrice() + " = " + nextShoe.getDiscountPrice()
                                + " and '" + nextShoe.getBrand() + " has a discount'\n");
                prev = shoppingCart.peek().getDiscountPrice(); // Getting discounted item price from top item of stack
                prevShoe = shoppingCart.pop();
            }
        }

    }
}

class ShoeData {
    private String brand;
    private Integer price;
    private Integer discount;

    public ShoeData(String brand, Integer price, Integer discount) {
        this.brand = brand;
        this.price = price;
        this.discount = discount;
    }

    public String getBrand() {
        return this.brand;
    }

    public Integer getPrice() {
        return this.price;
    }

    public Integer getDiscount() {
        return this.discount;
    }

    public Integer getDiscountPrice() {
        return ((100 - getDiscount()) * getPrice()) / 100;
    }

    @Override
    public String toString() {
        return "(" + "'" + brand + "'" + ", " + price + ", " + discount + ")";
    }
}

class Dictionary {
    private List<ShoeData> dictionary = new ArrayList<>();

    public int size() {
        return dictionary.size();
    }

    public void addShoe(ShoeData shoe) {
        ShoeData shoeEntry = new ShoeData(shoe.getBrand(), shoe.getPrice(), shoe.getDiscount());
        dictionary.add(shoeEntry);
    }

    public ShoeData getShoeData(int index) {
        int i = 0;
        for (ShoeData shoe : dictionary) {
            if (i == index)
                return shoe;
            i++;
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        int i = 0;
        for (ShoeData shoe : dictionary) {
            sb.append(shoe.toString());
            i++;
            if (i < size()) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

}

class OverflowException extends RuntimeException {
    public OverflowException(String message) {
        super(message);
    }
}

class UnderflowException extends RuntimeException {
    public UnderflowException(String message) {
        super(message);
    }

}