import java.lang.reflect.Array;

class Stack<T extends Comparable<T>> {
    private T[] stack;
    private int topIndex;

    // Constructor for Stack, creates an empty stack
    // @param class Component type
    // @param int maxSize maximum size of stack
    @SuppressWarnings("unchecked")
    public Stack(Class<T> clazz, int maxSize) {
        stack = (T[]) Array.newInstance(clazz, maxSize);
        topIndex = -1;
    }

    // Returns a boolean value for if the stack is empty
    public boolean isEmpty() {
        return topIndex == -1;
    }

    // Returns a boolean value for if the stack is full
    public boolean isFull() {
        return topIndex == (stack.length - 1);
    }

    // Add an item to the top of the stack
    public void push(T item) throws OverflowException {
        if (!isFull()) {
            topIndex++;
            stack[topIndex] = item;
        } else {
            throw new OverflowException("The stack is full, cannot push an element to a full stack.");
        }
    }

    // Remove an item from the top of the stack
    public T pop() throws UnderflowException {
        if (!isEmpty()) {
            T tmp = stack[topIndex];
            stack[topIndex] = null;
            topIndex--;
            return tmp;
        } else {
            throw new UnderflowException("The stack is empty, cannot pop an element from an empty stack.");
        }
    }

    // Show the top value of the stack
    public T peek() throws UnderflowException {
        if (!isEmpty()) {
            return stack[topIndex];
        } else {
            throw new UnderflowException("The stack is empty, cannot show top item.");
        }
    }

    // Clear the stack
    public void clear() {
        stack = null;
        topIndex = -1;
    }

    public int size() {
        return topIndex + 1;
    }

    public void reverseStack(Stack<T> stack, Queue<T> queue, Queue<T> uniqueSet) {
        // Step 1: Pop elements from the stack and enqueue into the queue
        while (!stack.isEmpty()) {
            T element = stack.pop();
            queue.enqueue(element);

            // Check if the element is in the unique set, if not, then add it to unique set
            if (!uniqueSet.contains(element)) {
                uniqueSet.enqueue(element);
            }
        }

        // Step 2: Dequeue elements from the queue and push into the stack
        while (!queue.isEmpty()) {
            T element = queue.dequeue();
            stack.push(element);
        }
    }

    public void contains(Stack<T> stack, Queue<T> queue) {
        // Step 1: Pop elements from the stack and enqueue into the queue
        while (!stack.isEmpty()) {
            T element = stack.pop();
            queue.enqueue(element);
        }
    }
}