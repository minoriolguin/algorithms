public class ArrayQueue<T> {
    private T[] queue;
    private int front, rear, size;

    @SuppressWarnings("unchecked")
    public ArrayQueue(int maxSize) {
        queue = (T[]) new Object[maxSize];
        front = 0;
        size = 0;
        rear = -1;
    }

    public boolean isFull() {
        return (size == queue.length);
    }

    public boolean isEmpty() {
        return (size == 0);
    }

    public int size() {
        return size;
    }

    public void enqueue(T item) {
        if (!isFull()) {
            rear = (rear + 1) % queue.length;            
            queue[rear] = item;
            size++;
        } else {
            throw new OverflowException("Queue is fullm cannot enqueue an item.");
        }
    }

    public T dequeue() {
        if (!isEmpty()) {
            T item = queue[front];
            front = (front + 1) % queue.length;
            size--;
            return item;
        } else {
            throw new UnderflowException("Queue is empty, cannot dequeue an item.");
        }

    }

    public T front() {
        if (isEmpty())
            return null;
        return queue[front];
    }

    public T rear() {
        if (isEmpty())
            return null;
        return queue[rear];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (int i = 0; i <= size() - 1; i++) {
            sb.append(queue[i]);
            if (i < size - 1) {
                sb.append(", ");
            }
        }
        sb.append(']');
        return sb.toString();
    }

    public void reverseQueue(Stack<Integer> stack, ArrayQueue<Integer> queue) {

            // Step 1: Dequeue elements from queue into stack
            while (!queue.isEmpty()) {
                stack.push(queue.dequeue());
            }

            // Step 2: Pop elements from stack and enqueue into queue
            while (!stack.isEmpty()) {
                queue.enqueue(stack.pop());
            }
        
    }

    public void divideQueue(ArrayQueue<Integer> queue) {
        ArrayQueue<Integer> tempQueue = new ArrayQueue<>(queue.size());

        Integer current = queue.dequeue();

        while (!queue.isEmpty()) {
            Integer next = queue.dequeue();

            if (current == 0) {
                tempQueue.enqueue(current);
            } else if (next == 0) {
                tempQueue.enqueue(next);
            } else if (current < next) {
                tempQueue.enqueue(next / current);
            } else {
                tempQueue.enqueue(current / next);
            }

            current = next;

            if (queue.isEmpty()) {
                tempQueue.enqueue(current);
            }
        }

        while (!tempQueue.isEmpty()) {
            queue.enqueue(tempQueue.dequeue());
        }
    }

    // Precondition: Queue must be sorted
    // Postcondition: Queue will no longer contain duplicate values
    public void removeDupes(ArrayQueue<Integer> queue) {
        ArrayQueue<Integer> tempQueue = new ArrayQueue<>(queue.size());

        Integer current = queue.dequeue();

        tempQueue.enqueue(current);

        while (!queue.isEmpty()) {
            Integer next = queue.dequeue();

            if (!(current == next)) {
                tempQueue.enqueue(next);
            }
        }

        // Enqueue elements back from tempList to queue
        while (!tempQueue.isEmpty()) {
            queue.enqueue(tempQueue.dequeue());
        }
    }

    public void sortQueue(ArrayQueue<Integer> queue) {
        if (queue.isEmpty())
            return;

        boolean switched = true;
        int size = queue.size();

        while (switched) {
            switched = false;
            Integer first = queue.dequeue();

            for (int i = 0; i < size - 1; i++) {
                Integer second = queue.dequeue();

                if (first.compareTo(second) > 0) {
                    queue.enqueue(second);
                    switched = true;
                } else {
                    queue.enqueue(first);
                    first = second;
                }

                if (i == size - 2) {
                    queue.enqueue(first); // Ensures the last element is enqueued back.
                }
            }

            if (switched) {
                queue.enqueue(queue.dequeue()); // Rotate the queue for the next pass.
                removeDupes(queue);
            }

            size--; // Optimizes by reducing the comparison range.
        }
    }

    
}
