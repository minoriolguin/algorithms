/*
 * Assignment 2 - Exercise 2 
 * Minori Olguin
 * UCID: 30035923 
 * 
 * Description: This program takes a hardcoded array and enqueues the values to a queue  
 *              then it returns the queue in reverse order, then it compares the current 
 *              front value by the subsequent value and saves the value into a temporary 
 *              queue, it divides the bigger number by the smaller number. If the smaller 
 *              number is 0, the current index is set to 0. The program then sorts the 
 *              array and removes duplicates, the array with unique values sorted in 
 *              ascending order is then returned.
 * 
 * Limitation: Cannot divide by 0,  if the program attempts to divide by 0 the value 
 *              of the number being divided is changed to 0 the same as what happens 
 *              in the assignment example.
 */


public class Main {
    public static void main(String args[]) {

        // Creating hardcoded values for the queue
        int[] intArray = { 3, 15, 0, 18, 4, 10, 2, 5, 25 };
        int size = intArray.length;

        // Initializing queue
        ArrayQueue<Integer> queue = new ArrayQueue<>(size);

        // Initializing stack
		Stack<Integer> stack = new Stack<>(Integer.class, size);

        // Filling the queue with hardcoded values from intArray
        for (int i : intArray) {
            queue.enqueue(i);
        }

        System.out.println("\nExercise 2: Reverse, divide, and sort queue\n");

        System.out.println("Step-1:\n");
        System.out.println("Initial queue - " + queue.toString() + "\n");

        queue.reverseQueue(stack, queue);
        System.out.println("Updated queue (after reverse) - " + queue.toString() + "\n\n");

        System.out.println("Step-2:\n");
        System.out.println("Updated queue (after reverse) - " + queue.toString() + "\n");

        queue.divideQueue(queue);
        System.out.println("Updated queue (after division) - " + queue.toString() + "\n\n");

        System.out.println("Step-3:\n");
        System.out.println("Updated queue (after division) - " + queue.toString() + "\n");

        queue.sortQueue(queue);
        System.out.println("Updated queue (after sorting) - " + queue.toString() + "\n\n");
    }
}
