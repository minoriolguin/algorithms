// Class underflow exception: handles empty queues
class UnderflowException extends RuntimeException {
    public UnderflowException(String message) {
        super(message);
    }

}