public class GraphTraversal<T> {

	public static void main(String[] args) {

		int[] input = { 0, 6, 6, 7, 7, 3, 3, 5, 4, 6 };
		int nodes = (input.length / 2) + 1;  // Number of nodes = number of edges + 1
		int src = 5;
		int dest = 4;

		Queue<Integer> edges = new Queue<>();
		Queue<Integer> path = new Queue<>();

		for (int i : input) {
			edges.enqueue(i);
		}		
		
		System.out.println("\nBonus Exercise: Graph Traversal\n");
		System.out.println("Input: Graph [edges = [" + edges.printQueue() + "] n = " + nodes + ", scr = " + src + ", dest = "
				+ dest + "]\n");

		path = edges.findPath(edges, src, dest);
		edges.printSummary(edges, path, src, dest);
	}

}


class Queue<T extends Comparable<T>> {

	@SuppressWarnings("hiding")
	private class Node<T> {
		private T value;
		private Node<T> next;
	}

	private Node<T> front = null;
	private Node<T> rear = null;

	// @param T item
	public void enqueue(T item) {
		Node<T> newNode = new Node<T>();
		newNode.value = item;
		newNode.next = null;

		if (rear != null) {
			rear.next = newNode; // Link the current rear to the new node.
		}
		rear = newNode;

		if (front == null) {
			front = newNode;
		}
	}

	// @returns tmp, the front value of the queue at time of function call
	public T dequeue() {
		if (front == null) {
			return null;
		}

		T tmp = front.value;
		front = front.next;
		return tmp;
	}

	public boolean isEmpty() {
		return front == null;

	}

	public boolean isFull() {
		return false;
	}

	public int size() {
		int size = 0;
		Node<T> current = front;
		while (current != null) {
			size++;
			current = current.next;
		}
		return size;
	}

	public void clear(Queue<Integer> queue) {
		while (!queue.isEmpty()) {
			queue.dequeue();
		}
	}

	// @param T item
	public boolean contains(T item) {
		Node<T> current = front; // Set current place to the front of the queue
		while (current != null) {
			if (current.value.equals(item)) {
				return true; // Item found
			}
			current = current.next; // Move to the next node
		}
		return false; // Item not found
	}

	public Queue<Integer> findPath(Queue<Integer> edges, int scr, int dest) {
		Integer current;
		Integer next;

		boolean pathExists = true;
		boolean currentFound = false;
		boolean pathFound = false;

		Queue<Integer> temp = new Queue<>(); // temp graph
		Queue<Integer> path = new Queue<>(); // path graph

		current = edges.dequeue(); // dequeues first graph item
		next = edges.dequeue(); // dequeues next graph item
								// Each group of two represents one node
		while (!pathFound && pathExists) {
			currentFound = edges.contains(scr);
			if (currentFound) {
				while (scr != current && !edges.isEmpty()) {   
					temp.enqueue(current);
					temp.enqueue(next);
					current = edges.dequeue();
					next = edges.dequeue(); // must be preformed in groups of two to represent a single graph node
					if (edges.isEmpty() && scr != current) {
						pathExists = false;
						while (!path.isEmpty()) {
							path.dequeue();
						}
					}
				}
				scr = next;
				if (path.isEmpty() && pathExists) {
					path.enqueue(current);
					path.enqueue(next);
				} else if (!path.isEmpty() && pathExists) {
					path.enqueue(next);
				}
			}
			while (!temp.isEmpty()) {
				edges.enqueue(temp.dequeue());
			}
			if (next == dest) {
				pathFound = true;
			}
		}

		return path;
	}

	public void printSummary(Queue<Integer> edges, Queue<Integer> path, int src, int dest) {
		
		boolean pathFound;

		if (path.size() == 0) {
			pathFound = false;
		} else {
			pathFound = true;
		}
		
		if (!pathFound) {
			System.out.println(
					"Output: false Explanation: There is no path from vertex " + src + " to any other vertex.\n");

		} else {
			System.out.println(
					"Output: true Explanation: There exists a path " + path.printPath() + " from vertex " + src
							+ " to vertex " + dest + ".\n");
		}
	}

	// Method to print the queue
	public String printQueue() {
		int i = 0;
		Node<T> current = front; // Set current place to front of the queue
		StringBuilder sb = new StringBuilder(); // Using string builder to produce desired format
		sb.append("("); // Start queue with (

		while (current != null) {
			sb.append(current.value); // Add current value to the String
			i++;
			if (current.next != null && ((i + 1) % 2) == 0) {
				sb.append(", "); // Use a ', ' in between each item
			} else if (current.next != null) {
				sb.append("), (");
			}
			current = current.next; // Move to the next node
		}

		sb.append(")"); // End queue with ]
		return sb.toString();
	}

	public String printPath() {
		Node<T> current = front; // Set current place to front of the queue
		StringBuilder sb = new StringBuilder(); // Using string builder to produce desired format
		sb.append("["); // Start queue with (

		while (current != null) {
			sb.append(current.value); // Add current value to the String
			if (current.next != null) {
				sb.append(" - "); // Use a ', ' in between each item
			} 
			current = current.next; // Move to the next node
		}

		sb.append("]"); // End queue with ]
		return sb.toString();
	}

}
