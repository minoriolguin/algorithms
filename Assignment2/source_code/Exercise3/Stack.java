/*
 * Assignment 2 - Exercise 3
 * Minori Olguin
 * UCID: 30035923 
 * 
 * Description: This program takes a hardcoded array and pushes all the values to a stack.
 * 				After the values are in the stack, the initial stack is then printed. 
 * 				Next, the program reverses the stack, while reversing the stack, the 
 * 				program adds all unique values to a queue named uniqueSet. Once the stack 
 * 				is reversed and the uniqueSet has all the unique values within the stack, 
 * 				the program prints the reversed stack. All the unique values from the unique 
 * 				set are added to the unique queue. The unique queue is then printed. 
 * 
 * Limitation: 
 */
import java.lang.reflect.Array;

public class Stack<T extends Comparable<T>> {
	private T[] stack;
	private int topIndex;

	// Constructor for Stack, creates an empty stack
	// @param class Component type
	// @param int maxSize maximum size of stack
	@SuppressWarnings("unchecked")
	public Stack(Class<T> clazz, int maxSize) {
		stack = (T[]) Array.newInstance(clazz, maxSize);
		topIndex = -1;
	}

	// Returns a boolean value for if the stack is empty
	public boolean isEmpty() {
		return topIndex == -1;
	}

	// Returns a boolean value for if the stack is full
	public boolean isFull() {
		return topIndex == (stack.length - 1);
	}

	// Add an item to the top of the stack
	public void push(T item) throws OverflowException {
		if (!isFull()) {
			topIndex++;
			stack[topIndex] = item;
		} else {
			throw new OverflowException("The stack is full, cannot push an element to a full stack.");
		}
	}

	// Remove an item from the top of the stack
	public T pop() throws UnderflowException {
		if (!isEmpty()) {
			T tmp = stack[topIndex];
			stack[topIndex] = null;
			topIndex--;
			return tmp;
		} else {
			throw new UnderflowException("The stack is empty, cannot pop an element from an empty stack.");
		}
	}

	// Show the top value of the stack
	public T peek() throws UnderflowException {
		if (!isEmpty()) {
			return stack[topIndex];
		} else {
			throw new UnderflowException("The stack is empty, cannot show top item.");
		}
	}

	// Clear the stack
	public void clear() {
		stack = null;
		topIndex = -1;
	}

	public int size() {
		return topIndex + 1;
	}

	public void reverseStack(Stack<T> stack, Queue<T> queue, Queue<T> uniqueSet) {
		// Step 1: Pop elements from the stack and enqueue into the queue
		while (!stack.isEmpty()) {
			T element = stack.pop();
			queue.enqueue(element);
		}

		// Step 2: Dequeue elements from the queue and push into the stack
		while (!queue.isEmpty()) {
			T element = queue.dequeue();
			stack.push(element);
		}


	}

	public void removeDupes(Stack<T> stack, Queue<T> queue, Queue<T> uniqueSet) {
		T element;
		// Step 1: Pop elements from the stack and check if they're in unique set, if not, enqueue into unique set
		while (!stack.isEmpty()) {
			element = stack.pop();
			if (!uniqueSet.contains(element))
				uniqueSet.enqueue(element);
		}

		// Step 2: transfer back into queue
		while (!uniqueSet.isEmpty()) {
			element = uniqueSet.dequeue();
			stack.push(element);
		}

		reverseStack(stack, queue, uniqueSet);

		// Step 3: pop from stack and enqueue into queue
		while (!stack.isEmpty()) {
			element = stack.pop();
			queue.enqueue(element);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (int i = topIndex; i >= 0; i--) {
			sb.append(stack[i]);
			if (i > 0) {
				sb.append(" - ");
			}
		}
		sb.append(']');
		return sb.toString();
	}

	public static void main(String[] args) {

		// Hardcoded input for the stack 
		int[] input = { 3, 5, 4, 8, 7, 0, 9, 8, 1, 3, 9, 2 };

		int maxSize = input.length;

		// Initializing unique set queue 
		Queue<Integer> uniqueSet = new Queue<>();

		// Initializing the main stack and main queue
		Stack<Integer> stack = new Stack<>(Integer.class, maxSize);
		Queue<Integer> uniqueQueue = new Queue<>();

		// Adding all the hardcoded integers to the stack 
		for (int i : input) {
			stack.push(i);
		}

		// Print title and the starting stack
		System.out.println("\nExercise 3: Stacks and Queues Implementation, Reverse and Iterate\n");
		System.out.println("Input Stack: " + stack.toString() + "\n");

		// Reverse the stack and store all unique values in a queue
		// Print the reversed stack
		stack.reverseStack(stack, uniqueQueue, uniqueSet);
		System.out.println("Output Reversed Stack: " + stack.toString() + "\n");

		// Add all the unique elements to a queue 
		stack.removeDupes(stack, uniqueQueue, uniqueSet);

		// Print the queue with unique values and reversed from the order of the input stack 
		System.out.println("Output Queue with unique values: " + uniqueQueue.printQueue() + "\n");
	}
}


// Class Queue<T>, implements a queue ADT
class Queue<T extends Comparable<T>> {

	@SuppressWarnings("hiding")
	private class Node<T> {
		private T value;
		private Node<T> next;
	}

	private Node<T> front = null;
	private Node<T> rear = null;

	// @param T item
	public void enqueue(T item) {
		Node<T> newNode = new Node<T>();
		newNode.value = item;
		newNode.next = null; 

		if (rear != null) {
			rear.next = newNode; // Link the current rear to the new node.
		}
		rear = newNode;

		if (front == null) {
			front = newNode;
		}
	}

	// @returns tmp, the front value of the queue at time of function call
	public T dequeue() {
		if (front == null) {
			return null;
		}

		T tmp = front.value;
		front = front.next;
		return tmp;
	}

	public boolean isEmpty() {
		return front == null;

	}

	public boolean isFull() {
		return false;
	}

	public int size() {
		int size = 0;
		Node<T> current = front;
		while (current != null) {
			size++;
			current = current.next;
		}
		return size;
	}

	// @param T item 
	public boolean contains(T item) {
		Node<T> current = front; // Set current place to the front of the queue
		while (current != null) {
			if (current.value.equals(item)) {
				return true; // Item found
			}
			current = current.next; // Move to the next node
		}
		return false; // Item not found
	}

	public Queue<T> addUniqueSetToQueue(Queue<T> uniqueSet, Queue<T> queue) {
		while (!uniqueSet.isEmpty()) {
			T current = uniqueSet.dequeue();
			queue.enqueue(current);
		}
		return queue;
	}

	// Method to print the queue
	public String printQueue() {
		Node<T> current = front; // Set current place to front of the queue
		StringBuilder sb = new StringBuilder(); // Using string builder to produce desired format
		sb.append("[ "); // Start queue with [

		while (current != null) {
			sb.append(current.value); // Add current value to the String 
			if (current.next != null) {
				sb.append(" - "); // Use a '-' in between each item  
			}
			current = current.next; // Move to the next node
		}

		sb.append(" ]"); // End queue with ]
		return sb.toString();
	}

}

class OverflowException extends RuntimeException {
	public OverflowException(String message) {
		super(message);
	}
}

class UnderflowException extends RuntimeException {
	public UnderflowException(String message) {
		super(message);
	}

}
