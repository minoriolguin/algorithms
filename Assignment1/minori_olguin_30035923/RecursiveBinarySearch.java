/*
 *  Minori Olguin
 *  UCID: 30035923
 *  CPSC 331 W24
 *  Assignment 1
 */

public class RecursiveBinarySearch {

    public static int recursiveBinarySearch(int[] arr, int low, int high, int target) {
        /*
         * int[] arr: Sorted array as input
         * int low: lower bound array index
         * int high: upper bound array index
         * target: Value that is being searched for in the array
         *
         * return: int [Either return the index of element that is being searched for or return -1 if not found]
         */
        System.out.println("Recursive Binary Search");
        int mid = (low + high)/ 2;     // Middle value for the index search 
        if (low > high) {
            return -1;      // If start index is greater than end index, program invalid, return -1
        }
        else if (target == arr[mid]) {
            return mid;
        }
        else if (target < arr[mid]) {
            return recursiveBinarySearch(arr, low, mid - 1, target); // Remove one from the mid value because mid value was eliminated as an option by first else if statement
        }
        else if (target > arr[mid]) {
            return recursiveBinarySearch(arr, mid + 1, high, target); // Add one to the mid value because mid value was eliminated as an option by first else if statement
        }
        return -1;
    }

       public static void main(String[] args) {
        int[] array = { 2, 3, 4, 10, 40 };
        int target = 10;
        // How the function is called
        int result = recursiveBinarySearch(array, 0, array.length - 1, target);

        if (result == -1) {
            System.out.println("Element not present");
        }
        else {
            System.out.println("Element found at index " + result);
        }
    }
}