/*
 *  Minori Olguin
 *  UCID: 30035923
 *  CPSC 331 W24
 *  Assignment 1
 */

public class IterativeBinarySearch {

    public static int iterativeBinarySearch(int[] arr, int target) {
        /*
         * int[] arr: Sorted array as input
         * target: Value that is being searched for in the array
         *
         * return: int [Either return the index of element that is being searched for or return -1 if not found]
         */
        System.out.println("Iterative Binary Search");
        int start = 0;          // Starting value of index search 
        int end = arr.length - 1;   // End value of index search 
        while(start <= end) {    // Creat loop condition
            int mid = (start + end)/ 2;     // Setting middle value
            if (arr[mid] < target) {    // If the target number is larger than the middle number
                start = mid + 1;        // Set new start value
            }
            else if (arr[mid] > target) {   // If the target number is smaller than the middle value
                    end = mid - 1;              // Set new end value
            }
            else if (arr[mid] == target) {   // If the target number is equal to middle value)
               return mid;
            }
        }
        return -1; 
    }

      public static void main(String[] args) {
        int[] array = { 2, 3, 4, 10, 40 };
        int target = 10;
        // How the function is called
        int result = iterativeBinarySearch(array, target);

        if (result == -1) {
            System.out.println("Element not present");
        }
        else {
            System.out.println("Element found at index " + result);
        }
    }
}