public class SortedIntArray {
    public static int[] sortedIntArray(int size){
        int[] arr = new int [ size ];
        if ( size < 0 ) {
            return arr;
        }
        else {
            for ( int i = 0; i < size; i++ ) {
                arr[ i ] = i + 1;
                System.out.println(arr[i]);
            }
            return arr;
        }
    }
}
