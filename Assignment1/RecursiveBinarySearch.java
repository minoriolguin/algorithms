public class RecursiveBinarySearch {

    public static int recursiveBinarySearch(int[] arr, int low, int high, int target) {
        /*
         * int[] arr: Sorted array as input
         * int low: lower bound array index
         * int high: upper bound array index
         * target: Value that is being searched for in the array
         *
         * return: int [Either return the index of element that is being searched for or return -1 if not found]
         */
        System.out.println("Recursive Binary Search");
        int start = low;        // Starting value of index search 
        int end = high;         // End value of index search
        int mid = (start + end)/ 2;     // Middle vale for the index search 

        if (start > end) {
            return -1;      // If start index is greater than end index, program invalid, return -1
        }
        else if (target == arr[mid]) {
            return mid;
        }
        else if (target < arr[mid]) {
            end = mid - 1;       // Remove one from the mid value because mid value was eliminated as an option by first else if statement
            return recursiveBinarySearch(arr, start, end, target);
        }
        else if (target > arr[mid]) {
            start = mid + 1;        // Add one to the mid value because mid value was eliminated as an option by first else if statement
            return recursiveBinarySearch(arr, start, end, target);
        }
        return -1;
    }

    public static int[] sortedIntArray(int size){
        int[] arr = new int [ size ];
        if ( size <= 0 ) {
            return arr;
        }
        else {
            for ( int i = 0; i < size; i++ ) {
                arr[ i ] = i + 1;
            }
            return arr;
        }
    }

    public static void main(String[] args) {

        int[] array = sortedIntArray(1);
        int target = 1;

        // How the function is called

        long startTime = System.nanoTime();

        int result = recursiveBinarySearch(array, 0, array.length - 1, target);

        long endTime = System.nanoTime();

        long totalTime = endTime - startTime;
        System.out.println("Total time for loop size " + array.length + ": " + totalTime + "\n");

        if (result == -1) {
            System.out.println("Element not present");
        }
        else {
            System.out.println("Element found at index " + result);
        }
    }
}