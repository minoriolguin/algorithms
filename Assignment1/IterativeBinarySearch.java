public class IterativeBinarySearch {

    public static int iterativeBinarySearch(int[] arr, int target) {
        /*
         * int[] arr: Sorted array as input
         * target: Value that is being searched for in the array
         *
         * return: int [Either return the index of element that is being searched for or return -1 if not found]
         */
        // System.out.println("Iterative Binary Search");
        int start = 0;          // Starting value of index search 
        int end = arr.length - 1;   // End value of index search 
        while(start <= end) {    // Creat loop condition
            int mid = (start + end)/ 2;     // Setting middle value
            if (arr[mid] < target) {    // If the target number is larger than the middle number
                start = mid + 1;        // Set new start value
            }
            else if (arr[mid] > target) {   // If the target number is smaller than the middle value
                    end = mid - 1;              // Set new end value
            }
            else {               // Else (if equal to mid value)
               return mid;
            }
        }
        return -1; 
    }

    public static int[] sortedIntArray(int size){
        int[] arr = new int [ size ];
        if ( size < 0 ) {
            return arr;
        }
        else {
            for ( int i = 0; i < size; i++ ) {
                arr[ i ] = i + 1;
            }
            return arr;
        }
    }

    public static void main(String[] args) {
        
        int[] array = sortedIntArray(1);
        int target = 1;

        // How the function is called
        long startTime = System.nanoTime();

        int result = iterativeBinarySearch(array, target);

        long endTime = System.nanoTime();

        long totalTime = endTime - startTime;
        System.out.println("Total time for loop size " + array.length + ": " + totalTime + "\n");

        if (result == -1) {
            System.out.println("Element not present");
        }
        else {
            System.out.println("Element found at index " + result);
        }
    }
}