package Heap;
import java.util.HashSet;
/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 3: Heap Sort
 */
import java.util.Random;
import java.util.Set;

public class Generator {

    private final static int UPPERBOUND = 1000;
    private Set<Integer> uniqueSet = new HashSet<>();

    public static int generateNum() {
        Random rand = new Random();
        int random_int = rand.nextInt(UPPERBOUND);
        return random_int;
    }

    public int[] generateRandomArray() {
        int[] randomArray = new int[UPPERBOUND];
        int i = 0;
        while (i < UPPERBOUND) {
            int rand = generateNum();
            if (!uniqueSet.contains(rand)) {
                randomArray[i] = rand;
                uniqueSet.add(rand);
                i++;
            }
        }
        return randomArray;
    }

    public int[] generateSortedArray() {
        int[] sortedArray = new int[UPPERBOUND];
        for (int i = 0; i < UPPERBOUND; i++ ) {
            sortedArray[i] = i;
        }
        return sortedArray;
    }
}
