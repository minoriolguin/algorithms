package Heap;
/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 3: Heap Sort
 */
public class HeapSortTask {
    public static void main(String[] args) {
        Generator generator = new Generator();

        System.out.println("\nMethod 1: Heapify\nSorting from an input array into a heap\n");

        int[] randomArray = generator.generateRandomArray();
        int[] sortedArray = generator.generateSortedArray();

        Heap randMaxHeap = new Heap(randomArray);
        Heap sortMaxHeap = new Heap(sortedArray);

        randMaxHeap.maxHeapifyRec();
        sortMaxHeap.maxHeapifyRec();

        System.out.println("Random array input swap count " + randMaxHeap.getSwapCount() + "\n");
        System.out.println("Sorted array input swap count " + sortMaxHeap.getSwapCount() + "\n");

        System.out.println("\nMethod 2: One By One\nBuilding a heap with insertion\n");

        Heap randomMaxHeapBuild = new Heap(randomArray.length);
        Heap sortedMaxHeapBuild = new Heap(sortedArray.length);

        for (int num : randomArray) {
            randomMaxHeapBuild.insert(num);
        }

        for (int num : sortedArray) {
            sortedMaxHeapBuild.insert(num);
        }

        System.out.println("Random array insertion swap count " + randomMaxHeapBuild.getSwapCount() + "\n");
        System.out.println("Sorted array insertion swap count " + sortedMaxHeapBuild.getSwapCount() + "\n");
    }
}
