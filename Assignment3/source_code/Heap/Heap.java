package Heap;

/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 3: Heap Sort
 */
public class Heap {
    private int[] Heap;
    private int size;
    private int maxsize;

    private int swapCount;

    public Heap(int maxsize) {
        this.maxsize = maxsize;
        this.size = 0;
        Heap = new int[this.maxsize + 1];
        Heap[0] = Integer.MAX_VALUE;
        swapCount = 0;
    }

    public Heap(int[] inputArray) {
        maxsize = inputArray.length + 1;
        this.Heap = new int[maxsize];
        Heap[0] = Integer.MAX_VALUE;
        swapCount = 0;

        System.arraycopy(inputArray, 0, this.Heap, 1, inputArray.length);
        size = inputArray.length;
    }

    public boolean isFull() {
        return size == maxsize;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public int getSwapCount() {
        return swapCount;
    }

    private int parent(int pos) {
        return pos / 2;
    }

    private int leftChild(int pos) {
        return (2 * pos);
    }

    private int rightChild(int pos) {
        return (2 * pos) + 1;
    }

    private void swap(int fpos, int spos) {
        int tmp = Heap[fpos];
        Heap[fpos] = Heap[spos];
        Heap[spos] = tmp;
        // System.out.println("Swapped " + Heap[fpos] + " with " + Heap[spos]);
        swapCount++;
    }

    public void maxHeapifyRec() {
        for (int pos = size() / 2; pos >= 1; pos--) {
            maxHeapify(pos);
        }
    }

    public void maxHeapify(int pos) {
        int current = pos;
        int left = leftChild(pos);
        int right = rightChild(pos);

        if (left <= size() && Heap[left] > Heap[current]) {
            current = left;
        }
        if (right <= size() && Heap[right] > Heap[current]) {
            current = right;
        }

        if (current != pos) {
            swap(pos, current);
            maxHeapify(current);
        }
    }

    public void insert(int element) {
        if (size >= maxsize) {
            return;
        }
        Heap[++size] = element;
        int current = size;

        while (Heap[current] > Heap[parent(current)]) {
            swap(current, parent(current));
            current = parent(current);
        }
    }

    public int remove() {
        if (isEmpty()) {
            throw new IllegalStateException("Heap is empty");
        }
        int popped = Heap[1];
        Heap[1] = Heap[size--];
        maxHeapify(1);
        return popped;
    }

    public int getMax() {
        if (isEmpty()) {
            throw new IllegalStateException("Heap is empty");
        }
        return Heap[1];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 1; i <= size(); i++) {
            sb.append(Heap[i]);
            if (i < size()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

}