package Heap;
/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 3: Heap Sort
 */
public class Main {
    
    public static void main(String[] args) {

        int[] input = {1, 2, 3, 4};

        Heap smolHeap = new Heap(input);

        System.out.println("\nSwaps for Method 1: \n");

        smolHeap.maxHeapifyRec();
        System.out.println(smolHeap.getSwapCount());

        smolHeap.remove(); // My function does the swaps every time remove is called if necessary
        smolHeap.remove();
        smolHeap.remove();
        smolHeap.remove();
        System.out.println(smolHeap.getSwapCount() + "\n");
        System.out.println("Total number of swaps: 3 + 2 = 5\n\n");

        System.out.println("\nSwaps for Method 2: \n");

        Heap otherSmolHeap = new Heap(input.length);

        for (int i : input) {
            otherSmolHeap.insert(i);
        }
        System.out.println(otherSmolHeap.getSwapCount());

        otherSmolHeap.remove(); // My function does the swaps every time remove is called if necessary
        otherSmolHeap.remove();
        otherSmolHeap.remove();
        otherSmolHeap.remove();
        System.out.println(otherSmolHeap.getSwapCount() + "\n");
        System.out.println("Total number of swaps: 4 + 1 = 5\n\n");

    }
}
