package HashTable;

/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 2: Designing a Hash Table for School Database Management
 */
import java.util.LinkedList;
import java.util.ListIterator;

@SuppressWarnings("rawtypes")
public class HashTableSC<T extends Hashable> implements HashTableInterface<T> {
    private LinkedList<T>[] hashTable;
    private HashValue hashFunc;

    @SuppressWarnings("unchecked")
    public HashTableSC(HashValue hashFunc, int tableSize) {
        hashTable = new LinkedList[tableSize];
        this.hashFunc = hashFunc;
    }

    /*
     * @precond ID is an integer and name is a string composed of alphanumeric
     * characters
     * 
     * @postcond If the hash table does not contain a Student of this name, then a
     * Student with attributes ID and name is added to the appropriate linked list
     * in the hash table. If a Student of this name is already in the hash table,
     * then this Student has their name updated to the inputted name.
     */
    @Override
    public void insert(T item) {
        Student student = (Student) item;
        int index = hashFunc.hash(student.key());
        if (hashTable[index] == null) {
            hashTable[index] = new LinkedList<T>();
        }
        for (int i = 0; i < hashTable[index].size(); i++) {
            Student current = (Student) hashTable[index].get(i);
            if (current.key().equals(student.key())) {
                current.setName(student.getName());
                return; 
            }
        }
        hashTable[index].add(item);
    }

    /*
     * @precond ID is an integer.
     * 
     * @postcond If the hash table contains a Student with this ID, this Student is
     * removed from the hash table. Otherwise, a message is printed indicating that
     * no Student with this name was found in the hash table.
     */
    @Override
    public void delete(int id) {
        int index = hashFunc.hash(id);
        if (search(id)) {
            for (int i = 0; i < hashTable[index].size(); i++) {
                Student student = (Student) hashTable[index].get(i);
                if (student.key() == id) {
                    hashTable[index].remove(i);
                    System.out.println("Student with ID: '" + id + "' was removed from the database. \n");
                    return; 
                }
            }
        }
        else {
            System.out.println("Cannot remove the student with ID: '" + id + "' because they do not exist in the database. \n");
        }
    }

    /*
     * @precond ID is an integer.
     * 
     * @postcond If the hash table contains a student by this ID, then return true.
     * Otherwise, return false.
     */
    @Override
    public boolean search(int id) {
        int i = hashFunc.hash(id);
        if (hashTable[i] == null)
            return false;
        for (T student : hashTable[i]) {
            if (student.key() == id) {
                return true;
            }
        }
        return false;
    }

    /*
     * @precond ID is an integer.
     * 
     * @postcond If the hash table contains a student with this ID, return the
     * corresponding name. Otherwise, print a message indicating that no student
     * with
     * this ID was found in the hash table.
     */
    @Override
    public String retrieve(int id) {
        int i = hashFunc.hash(id);

        String notFound = "The student with ID: '" + id + "' was NOT found in the database. :( \n";
        String found = "The student with ID: '" + id + "' is named: ";
        
        if (hashTable[i] == null)
            return notFound;

        for (T student : hashTable[i]) {
            if (student.key() == id) {
                return found + ((Student) student).getName() + "\n";
            }
        }
        return notFound;
    }

    @Override
    public void clear() {
        for (int i = 0; i < hashTable.length; i++) {
            hashTable[i] = null;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hashTable.length; i++) {
            LinkedList<T> tableRow = hashTable[i];
            sb.append(i);
            sb.append(": [");
            if (tableRow != null) {
                ListIterator<T> iterator = tableRow.listIterator();
                while (iterator.hasNext()) {
                    Student student = (Student) iterator.next();
                    sb.append(student.key()).append(":").append(student.getName());

                    if (iterator.hasNext()) {
                        sb.append(", ");
                    }
                }
            }
            sb.append("]\n");
        }
        return sb.toString();
    }
}
