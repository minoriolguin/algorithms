package HashTable;

/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 2: Designing a Hash Table for School Database Management
 */
public class HashValue<T extends Comparable>{
    private final int LEN = 8;
    /*
     * Hash Function
     * @precond ID is an integer
     * @postcond ID modulo LEN is returned
     */
    public int hash(Integer key) {
        int hashValue = key;
        hashValue %= LEN;
        if (hashValue < 0) {
            hashValue += LEN;
        }
        return hashValue;
    }
}
