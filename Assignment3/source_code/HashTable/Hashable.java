package HashTable;
/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 2: Designing a Hash Table for School Database Management
 */
public interface Hashable<T extends Comparable> {
    public Integer key();
    
} 