package HashTable;
/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 2: Designing a Hash Table for School Database Management
 */
@SuppressWarnings("rawtypes")
public interface HashTableInterface <T extends Hashable>{
    
    public void insert(T item);

    public void delete(int ID);

    public boolean search(int ID);

    public String retrieve(int ID);

    public void clear();
}
