package HashTable;
/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 2: Designing a Hash Table for School Database Management
 */

public class Main {
    public static void main(String args[]){
        final int LEN = 8;

        HashValue<Integer> hashFunction = new HashValue<Integer>();
        HashTableSC<Student> database = new HashTableSC<Student>(hashFunction, LEN);
    
        database.insert(new Student("Bob", 20500120));
        database.insert(new Student("Alice", 20700200));
        database.insert(new Student("Cathy", 30100230));
        database.insert(new Student("Ali", 20200156));

        System.out.println(database.toString());

        database.insert(new Student("Bobby", 20500120));

        int searchId = 20500120;

        if (database.search(searchId)) {
            System.out.println("The ID: '" + searchId + "' was found in the database. :) \n");
        }
        else {
            System.out.println("The ID: '" + searchId + "' was NOT found in the database. :( \n");
        }

        int retrieveId = 20700200;
        System.out.println(database.retrieve(retrieveId));

        database.delete(retrieveId);

        database.delete(retrieveId);

        System.out.println(database.retrieve(retrieveId));

        System.out.println(database.toString());
    }
}
