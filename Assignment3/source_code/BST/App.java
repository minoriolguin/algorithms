/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 1: Binary Search Tree
 * Code not my own, created by xChart
 */
import java.io.IOException;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.BitmapEncoder.BitmapFormat;
import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;

public class App {

	public void makeChart(double[] xData, double[] yData) throws IOException {

    	// Create Chart
		XYChart chart = new XYChartBuilder().width(600).height(500).title("BST Time/Height Chart").xAxisTitle("Time (nanoseconds)").yAxisTitle("Height (of bst)").build();

    	// Customize Chart
    	chart.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Scatter);
    	chart.getStyler().setLegendVisible(false);
		chart.getStyler().setMarkerSize(6);
		chart.addSeries("BST data", xData, yData);
    	
		// Show it
    	new SwingWrapper(chart).displayChart();
    	
    	// or save it in high-res
    	BitmapEncoder.saveBitmapWithDPI(chart, "./Sample_Chart_300_DPI", BitmapFormat.PNG, 300);
	}
	
}