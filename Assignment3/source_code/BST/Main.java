/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 1: Binary Search Tree
 */
import java.util.ArrayList;


class Main {
    public static void main(String args[]) throws Exception {
        BinarySearchTree<Integer> bst = new BinarySearchTree<Integer>();
        ArrayList<Long> timeList = new ArrayList<Long>();
        ArrayList<Integer> heightList = new ArrayList<Integer>();

        int distinctNodes = 100; // represents the numner of desired distinct nodes, [100, 200]
        int upperbound = 300; // upperbound on the range of the random numnbers generated
        int newNodes = 100; // k is equal to newNodes which is how many nodes we will add in step 3, [50,150]
        int nodes = 0; // current number of distinct nodes
        int randNum;

        // Step 1: create a BST containing distinctNodes (m) amount of nodes
        while (nodes < distinctNodes) { // While nodes is less than distinct nodes, continue to loop
            randNum = RandomNumGenerator.generateNum(upperbound); // generate a new random number
            if (!bst.contains(randNum)) { // check if randNum is already in the BST
                bst.add(randNum); // if the number is distinct, add randNum to BST
                nodes++; // incremenet number of nodes
            }
        }

        // Step 2: Remove all odd numbers from BST
        bst.removeOdds();
        distinctNodes = nodes = bst.size();

        long totalTime = 0;

        // Step 3: Insert k new random integers that are not already in the BST
        while (nodes < distinctNodes + newNodes) {
            randNum = RandomNumGenerator.generateNum(upperbound); // generate a new random number
            if (!bst.contains(randNum)) { // check if randNum is already in the BST

                int height = bst.height(); // find the height of the binary tree
                heightList.add(height);

                long start = System.nanoTime();
                bst.add(randNum); // add randNum to BST, and record time
                long end = System.nanoTime();
                totalTime = end - start;
                timeList.add(totalTime);
            }
            nodes = bst.size();
        }

        // Making the chart and input for chart
        double[] yData = new double[heightList.size()];
        double[] xData = new double[timeList.size()];

        int i = 0;
        int j = 0;

        for (int height : heightList) {
            yData[i] = height;
            i++;
        }
        for (long time : timeList) {
            xData[j] = time;
            j++;
        }

        // This is how to populate the chart, xChart jar must be installed on your pc or it will produce a compilation error
        // App app = new App();
        // app.makeChart(xData, yData);
    }
}
