/*
 * Minori Olguin
 * UCID: 30035923
 * CPSC 331 - Data Structures and Algorithms
 * Assignment 5 - Problem 1: Binary Search Tree
 */
import java.util.Random;

public class RandomNumGenerator {
    
    public static int generateNum(int upperbound) {
        Random rand = new Random();
        int random_int = rand.nextInt(upperbound);
        return random_int;
    }

    public static int generateEvenNum(int upperbound) {
        Random rand = new Random();
        upperbound = upperbound/2;
        int random_int = rand.nextInt((upperbound));
        return random_int * 2;
    }
}
